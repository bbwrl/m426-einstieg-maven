package ch.bbw.einstieg.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
    private String title;
    private String body;

    public Message(String title) {
        this.title = title;
        this.body = "";
    }
}
