package ch.bbw.einstieg;

import ch.bbw.einstieg.model.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RESTController {

    @GetMapping("hello")
    public Message hello() {
        return new Message("Hello");
    }
}
