package ch.bbw.einstieg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Einstieg426Application {

    public static void main(String[] args) {
        SpringApplication.run(Einstieg426Application.class, args);
    }

}
