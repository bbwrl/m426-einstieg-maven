package ch.bbw.einstieg;

import ch.bbw.einstieg.model.Message;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMessage {

    @Test
    public void testConstructor() {
        String testMessage = "testmessage";
        Message message = new Message(testMessage);
        assertEquals(testMessage, message.getTitle());
        assertEquals("", message.getBody());
    }

    @Test
    public void testGetterAndSetter() {
        Message message = new Message("");

        String testMessage = "testmessage";
        String testBody = "testbody";
        message.setTitle(testMessage);
        message.setBody(testBody);
        assertEquals(testMessage, message.getTitle());
        assertEquals(testBody, message.getBody());

    }
}
